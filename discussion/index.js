// basic imports

const express = require("express");
const mongoose = require("mongoose");
// allows us to use contents of "taskRoutes.js" in the routes folder
const taskRoutes= require("./routes/taskRoutes.js");

// Server set-up
const app = express();
const port = 3000;

// MongoDB Connection
mongoose.connect("mongodb+srv://jpunzalan:iloveNZ@wdc028-course-booking.0tib1.mongodb.net/b190-to-do?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));

db.once("open", ()=>console.log("we're connected to the database"));

app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Allows all the task routes created in the taskRoutes.js files to use "/tasks" route. Aside from using the localhost:3000, isasama na yung iaccess like /tasks or /users plus yung endpoint
app.use("/tasks", taskRoutes)

app.listen(port, ()=> console.log(`Server running at port: ${port}`));
