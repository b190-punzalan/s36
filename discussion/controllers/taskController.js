// contains functions and business logics of our express js applic; andito na lahat ng ways/commands we can manage our database
// all operations it can do will be placed in this file
// allows us to use the contents of "task.js" file in the "models" folder
const Task=require("../models/task.js");
// kaya may ../ kasi tumaas ng folder (pathing) tapos iaccess yung mismong file 

// defines the functions to be used in the "taskRoutes.js". Ang need mo lang ay yung function itself
module.exports.getAllTasks=()=>{
	return Task.find({}).then(result=>{
		return result;
	});
};
// since need natin na magamit ito ni taskRoustes.js natin, balik tayo kay taskRoutes.js. iimport natin sa file na yun ang laman naman ng controller folder at taskController na file (see line 9 and 10  of taskRoutes.js)

// function for creating 
module.exports.createTask=(requestBody)=>{
	let newTask=new Task({
		name: requestBody.name
	});
	return newTask.save().then((task, error)=>{
		if (error){
			console.log(error)
			return false;
		}else{
			return task;
		}
	})
}

// bakit false yung return? best practice to save resources


// function for deleting a task
/*
Look for the task using "findByIdAnd Remove" with the corresponding id provided/passed from the URI/route

Delete the task
	- wait for the deletion to complete and then if there are any errors, print in the console
	- if there are no errors, return the removed task

*/

module.exports.deleteTask=(taskId)=>{
	return Task.findByIdAndRemove(taskId).then((result, error)=>{
		if (error){
			console.log(error)
			return false;
		}else{
			return result;
		}
	})
}
// di mo need magsave kasi yung delete task, kasabay na siya nung findByIdAndRemove which is a mongoose method that searches for the documents using their _id properties; after finding, the job of this method is to remove the document

// for updating a task
/*
Business Logic
1. get the task with the id using the mongoose method "findById"
2. replace the task's name returned from the database with the "name" property from the request body
3. save the task

*/
module.exports.updateTask=(taskId, newContent)=>{
	return Task.findById(taskId).then((result, error)=>{
		if(error){
			console.log(error);
			return false;
		}else{
			result.name=newContent.name;
			return result.save().then((updatedTask, error)=>{
				if (error){
					console.log(error);
					return false
				}else{
					return updatedTask;
				}
			})
		}
	})
};

// Business Logic for Activity updating a task status to "complete"
/*
		1. Get the task with the id using the Mongoose method "findById"
		2. Change the status of the document to complete (hardcoded OR using requestBody)
		3. Save the task
*/



// Activity
// 1. Create a route for getting a specific task.
// 2. Create a controller function for retrieving a specific task.
// 3. Return the result back to the client/Postman.
// 4. Process a GET request at the "/tasks/:id" route using postman to get a specific task.
// 5. Create a route for changing the status of a task to "complete".
// 6. Create a controller function for changing the status of a task to "complete".
// 7. Return the result back to the client/Postman.
// 8. Process a PUT request at the "/tasks/:id/complete" route using postman to update a task.
// 9. Create a git repository named S36.
// 10. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code

module.exports.getTask=(taskId)=>{
	return Task.findById(taskId).then(result=>{
		return result;
	});
};

module.exports.updateStatus=(statusId, newStatus)=>{
	return Status.findById(statusId).then((newStatus, error)=>{
		if(error){
			console.log(error);
			return false;
		}else{
			result.status=newStatus.status;
			return result.save().then((updatedStatus, error)=>{
				if (error){
					console.log(error);
					return false
				}else{
					return updatedStatus;
				}
			})
		}
	})
};


