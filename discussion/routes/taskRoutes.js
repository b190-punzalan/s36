// contains all endpoints for our applic
// app.js/index.js should only be concerned with info about the server
// we have to use the Router method inside express

const express = require("express");
// allows us to access HTTP method middlewares that make it easier to create routing system
const router = express.Router();

// this allows us to use the contents of taskControllers.js in "controllers" folder
const taskController= require("../controllers/taskController.js")
// route to get all tasks
/*
get request to be sent at localhost:3000/tasks
*/
router.get("/", (req,res)=>{
	taskController.getAllTasks().then(resultFromController=>res.send(resultFromController));
});
// Since the controller did not send any response, the task falls to the routes to send a response that is reeived from the controllers and send it to the frontend. Sa getAllTasks, kinuha lang natin pero hindi natin nireturn. So may .then para masend as a response 

// mini activity
/*
create "/" route that will process the request and send the response based on the createTask function inside the "taskController.js"
*/
router.post("/", (req,res)=>{
	console.log(req.body);
	taskController.createTask(req.body).then(resultFromController=>res.send(resultFromController));
});


// route for deleting a task
router.delete("/:id", (req, res)=>{
	taskController.deleteTask(req.params.id).then(resultFromController=>res.send(resultFromController));
});

// route for updating a task
/*
req.params.id retrieves the taskId of the document to be updated 
req.body- determines what updates are to be done in the document coming from the request's body
*/
router.put("/:id", (req, res)=>{
	taskController.updateTask(req.params.id, req.body).then(resultFromController=>res.send(resultFromController));
});


// Activity
router.get("/:id", (req,res)=>{
	taskController.getTask(req.params.id).then(resultFromController=>res.send(resultFromController));
});

router.put("/:id", (req, res)=>{
	taskController.updateStatus(req.params.id, req.body).then(resultFromController=>res.send(resultFromController));
});



// module.exports= allow us to export the file where it is inserted to be used by other files such as app.js/index.js
module.exports=router;


